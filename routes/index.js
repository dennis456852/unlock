const express = require('express');
const router = express.Router();

const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545')

const fs = require('fs');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/unlock1', function (req, res, next) {
  let { account } = req.body;

  let password = fs.readFileSync('./passwords/' + account + '.txt').toString()
  console.log(password)

  web3.eth.personal.unlockAccount(account, password, 60)
    .then(function (result) {
      res.send('true')
    })
    .catch(function (err) {
      res.send('false')
    })

})

router.post('/unlock2', function (req, res, next) {
  let { account } = req.body;

  let password = require('../passwords/pwds.json')[account]
  console.log(password)
  
  web3.eth.personal.unlockAccount(account, password, 60)
    .then(function (result) {
      res.send('true')
    })
    .catch(function (err) {
      res.send('false')
    })

})

module.exports = router;
